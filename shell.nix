{
  pkgs ? import <nixpkgs> { },
}:
pkgs.mkShell {
  buildInputs = with pkgs; [
    # Rust
    cargo
    rustc
    clippy
    rust-analyzer
    rustfmt

    # Maelstrom
    openjdk19_headless
    gnuplot
  ];
  RUST_SRC_PATH = "${pkgs.rust.packages.stable.rustPlatform.rustLibSrc}";
}
