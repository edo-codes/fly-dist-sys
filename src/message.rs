use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Deserialize, Serialize, Debug)]
pub struct Message {
    pub src: String,
    pub dest: String,
    pub body: Body,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct Body {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub msg_id: Option<u64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub in_reply_to: Option<u64>,
    #[serde(flatten)]
    pub contents: BodyContents,
}

#[derive(Deserialize, Serialize, Debug)]
#[serde(tag = "type", rename_all = "snake_case")]
pub enum BodyContents {
    Init {
        node_id: String,
        node_ids: Vec<String>,
    },
    InitOk {},
    Echo {
        echo: String,
    },
    EchoOk {
        echo: String,
    },
    Generate {},
    GenerateOk {
        id: String,
    },
    Broadcast {
        message: u64,
    },
    BroadcastOk {},
    Read {},
    ReadOk {
        messages: Vec<u64>,
    },
    Topology {
        topology: HashMap<String, Vec<String>>,
    },
    TopologyOk {},
}

impl BodyContents {
    pub const fn get_type(&self) -> &'static str {
        match self {
            Self::Init { .. } => "init",
            Self::InitOk { .. } => "init_ok",
            Self::Echo { .. } => "echo",
            Self::EchoOk { .. } => "echo_ok",
            Self::Generate { .. } => "generate",
            Self::GenerateOk { .. } => "generate_ok",
            Self::Broadcast { .. } => "broadcast",
            Self::BroadcastOk { .. } => "broadcast_ok",
            Self::Read { .. } => "read",
            Self::ReadOk { .. } => "read_ok",
            Self::Topology { .. } => "topology",
            Self::TopologyOk { .. } => "topology_ok",
        }
    }
}
