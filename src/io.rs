use anyhow::{Context, Result};
use tokio::io::{AsyncBufReadExt, AsyncWriteExt, BufReader, BufWriter, Stdin, Stdout};
use tokio_stream::{wrappers::LinesStream, StreamExt};

use crate::message::Message;

pub struct Input {
    stdin: LinesStream<BufReader<Stdin>>,
}

impl Input {
    pub fn new() -> Self {
        Self {
            stdin: LinesStream::new(BufReader::new(tokio::io::stdin()).lines()),
        }
    }

    pub async fn read_message(&mut self) -> Result<Option<Message>> {
        while let Some(line) = self.stdin.next().await {
            let line = line.context("Read line from async stdin into String")?;
            if line.trim().is_empty() {
                continue;
            }
            return serde_json::from_str(&line).context("Parse message JSON");
        }
        Ok(None)
    }
}

pub struct Output {
    stdout: BufWriter<Stdout>,
}

impl Output {
    pub fn new() -> Self {
        Self {
            stdout: BufWriter::new(tokio::io::stdout()),
        }
    }

    pub async fn write_message(&mut self, msg: &Message) -> Result<()> {
        let line = serde_json::to_string(msg).context("Serialize message JSON to string")?;
        self.stdout
            .write_all(line.as_bytes())
            .await
            .context("Write string to async stdout")?;
        self.stdout
            .write_all(&[b'\n'])
            .await
            .context("Write newline to async stdout")?;
        self.stdout.flush().await.context("Flush async stdout")?;
        Ok(())
    }
}
