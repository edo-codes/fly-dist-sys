#![allow(clippy::new_without_default)]
#![allow(clippy::missing_errors_doc)]
#![allow(clippy::similar_names)]
#![allow(clippy::redundant_pub_crate)]

mod io;
mod message;
mod node;

use futures::FutureExt;
use io::{Input, Output};
use message::{Body, BodyContents, Message};
use node::Node;
use tokio::{sync::mpsc, task, try_join};

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let (tx_input, rx_input) = mpsc::channel(100);
    let (tx_output, mut rx_output) = mpsc::channel(100);

    let task_input = task::spawn(async move {
        let mut input = Input::new();
        while let Some(msg) = input.read_message().await? {
            tx_input.send(msg).await?;
        }
        Ok(())
    })
    .map(|r| r?);

    let task_output = task::spawn(async move {
        let mut output = Output::new();
        while let Some(ref msg) = rx_output.recv().await {
            output.write_message(msg).await?;
        }
        Ok(())
    })
    .map(|r| r?);

    let task_node = Node::run(rx_input, tx_output);

    try_join!(task_input, task_output, task_node)?;
    Ok(())
}
