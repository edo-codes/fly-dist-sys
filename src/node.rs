use crate::{Body, BodyContents, Message};
use anyhow::{bail, ensure, Context, Result};
use futures::future::try_join_all;
use tokio::sync::mpsc::{Receiver, Sender};

#[allow(clippy::struct_field_names)]
pub struct Node {
    node_id: String,
    tx_output: Sender<Message>,
    next_msg_id: u64,
    neighbors: Option<Vec<String>>,

    // Challenge #2
    next_unique_id: u64,

    // Challenge #3
    received_broadcast_messages: Vec<u64>,
}

impl Node {
    pub async fn run(mut rx_input: Receiver<Message>, tx_output: Sender<Message>) -> Result<()> {
        let Some(init_msg) = rx_input.recv().await else {
            anyhow::bail!("No first message in input channel");
        };
        let BodyContents::Init { ref node_id, .. } = init_msg.body.contents else {
            anyhow::bail!("First message was not of type init");
        };
        let mut node = Self {
            node_id: node_id.clone(),
            next_msg_id: 0,
            tx_output,
            neighbors: None,

            next_unique_id: 0,
            received_broadcast_messages: Vec::new(),
        };
        node.send_reply(&init_msg, BodyContents::InitOk {}).await?;

        while let Some(msg) = rx_input.recv().await {
            node.handle_message(&msg).await?;
        }

        Ok(())
    }

    async fn handle_message(&mut self, msg: &Message) -> Result<()> {
        ensure!(msg.dest == self.node_id);
        match msg.body.contents {
            BodyContents::Echo { ref echo } => {
                // Echo contents back to src
                self.send_reply(msg, BodyContents::EchoOk { echo: echo.clone() })
                    .await?;
                Ok(())
            }
            BodyContents::Generate {} => {
                // Generate globally unique id
                self.send_reply(
                    msg,
                    BodyContents::GenerateOk {
                        id: format!("{}-{}", self.node_id, self.next_unique_id),
                    },
                )
                .await?;
                self.next_unique_id = self
                    .next_unique_id
                    .checked_add(1)
                    .context("Next_unique_id overflowed")?;
                Ok(())
            }
            BodyContents::Broadcast { message } => {
                // Record message and broadcast it to neighbors
                if !self.received_broadcast_messages.contains(&message) {
                    // If message was received before, it must also already have been broadcasted
                    self.received_broadcast_messages.push(message);
                    self.broadcast_message(message, &msg.src)
                        .await
                        .context("Broadcast received message to neighbors")?;
                }
                self.send_reply(msg, BodyContents::BroadcastOk {}).await?;
                Ok(())
            }
            BodyContents::Read {} => {
                // Reply with received broadcast messages
                let reply = BodyContents::ReadOk {
                    messages: self.received_broadcast_messages.clone(),
                };
                self.send_reply(msg, reply).await?;
                Ok(())
            }
            BodyContents::Topology { ref topology } => {
                // Record neighbors
                self.neighbors = Some(
                    topology
                        .get(&self.node_id)
                        .context("Topology message should contain current node")?
                        .clone(),
                );
                self.send_reply(msg, BodyContents::TopologyOk {}).await?;
                Ok(())
            }
            BodyContents::BroadcastOk { .. } => {
                // TODO: Verify ok messages
                Ok(())
            }

            BodyContents::Init { .. }
            | BodyContents::InitOk { .. }
            | BodyContents::EchoOk { .. }
            | BodyContents::GenerateOk { .. }
            | BodyContents::ReadOk { .. }
            | BodyContents::TopologyOk { .. } => {
                bail!(format!(
                    "Unexpected message type \"{}\"",
                    msg.body.contents.get_type()
                ))
            }
        }
    }

    async fn send_reply(
        &mut self,
        received_message: &Message,
        contents: BodyContents,
    ) -> Result<()> {
        let msg_id = Some(self.get_msg_id()?);
        let reply = Message {
            src: self.node_id.to_string(),
            dest: received_message.src.to_string(),
            body: Body {
                contents,
                in_reply_to: received_message.body.msg_id,
                msg_id,
            },
        };
        let reply_type = reply.body.contents.get_type();
        self.send_message(reply).await.with_context(|| {
            format!(
                "Reply to {} message with {} message",
                received_message.body.contents.get_type(),
                reply_type
            )
        })
    }

    async fn send_message(&mut self, msg: Message) -> Result<()> {
        self.tx_output.send(msg).await.map_err(Into::into)
    }

    fn get_msg_id(&mut self) -> Result<u64> {
        let msg_id = self.next_msg_id;
        self.next_msg_id = self
            .next_msg_id
            .checked_add(1)
            .context("Next_msg_id overflowed")?;
        Ok(msg_id)
    }

    async fn broadcast_message(&mut self, message: u64, src: &str) -> Result<()> {
        let Some(neighbors) = self.neighbors.clone() else {
            anyhow::bail!("Neighbors should be known when receiving broadcast message")
        };
        try_join_all(neighbors.into_iter().map(|neighbor_id: String| async {
            if neighbor_id != src {
                self.tx_output
                    .send(Message {
                        src: self.node_id.to_string(),
                        dest: neighbor_id,
                        body: Body {
                            msg_id: None,
                            in_reply_to: None,
                            contents: BodyContents::Broadcast { message },
                        },
                    })
                    .await?;
            }
            anyhow::Ok(())
        }))
        .await?;
        Ok(())
    }
}
