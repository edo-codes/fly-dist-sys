#! /bin/sh

set -e

cargo build --release
./maelstrom/maelstrom test -w echo --bin ./target/release/fly-dist-sys --node-count 1 --time-limit 10
./maelstrom/maelstrom test -w unique-ids --bin ./target/release/fly-dist-sys --time-limit 30 --rate 1000 --node-count 3 --availability total --nemesis partition
./maelstrom/maelstrom test -w broadcast --bin ./target/release/fly-dist-sys --node-count 1 --time-limit 20 --rate 10
./maelstrom/maelstrom test -w broadcast --bin ./target/release/fly-dist-sys --node-count 5 --time-limit 20 --rate 10
